import { _PAGE, _STATIC } from "../routes/routesConst";

export const menuData = [
  {
    id: 1,
    name: "Inicio",
    route: _STATIC.HOME,
  },
  {
    id: 2,
    name: "Usuarios",
    route: _PAGE.USERS,
  },
  {
    id: 3,
    name: "Mis Fichajes",
    route: _PAGE.SIGNING,
  },
  {
    id: 4,
    name: "Mis Ausencias",
    route: _PAGE.ABSENCE,
  },
  {
    id: 5,
    name: "Mis Tareas",
    route: _PAGE.TASKS,
  },
];
