import { _PAGE, _STATIC } from "../routes/routesConst";
import Register from "../components/Register";

import HomePage from "../pages/HomePage";
import { UserPage } from "../pages/UserPage";
import { SigningPage } from "../pages/SigningPage";
import { AbsencePage } from "../pages/AbsencePage";
import { TaskPage } from "../pages/TaskPage";
import Profile from "../components/Profile";

export const routesData = [
  { path: _STATIC.SH_HOME, component: HomePage },
  { path: _STATIC.HOME, component: HomePage },
  { path: _STATIC.REGISTER, component: Register },
  { path: _STATIC.PROFILE, component: Profile },
  { path: _PAGE.USERS, component: UserPage },
  { path: _PAGE.TASKS, component: TaskPage },
  { path: _PAGE.SIGNING, component: SigningPage },
  { path: _PAGE.ABSENCE, component: AbsencePage },
];
