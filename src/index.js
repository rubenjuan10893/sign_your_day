import React from "react";
import ReactDOM from "react-dom";
import "./index.css";
import App from "./App";
import { CookiesProvider } from "react-cookie";
import { UserProvider } from "./context/userContext";
import { SnackbarProvider } from "notistack";
import { SignProvider } from "./context/signContext";
import { TempProvider } from "./context/tempContext";

ReactDOM.render(
  <React.StrictMode>
    <CookiesProvider>
      <SignProvider>
        <SnackbarProvider>
          <UserProvider>
            <TempProvider>
              <App />
            </TempProvider>
          </UserProvider>
        </SnackbarProvider>
      </SignProvider>
    </CookiesProvider>
  </React.StrictMode>,
  document.getElementById("root")
);
