import React, {
  createContext,
  Fragment,
  useContext,
  useEffect,
  useState,
} from "react";
import { _DASHBOARD, _PAGE, _STATIC } from "./routes/routesConst";
import { Fab, makeStyles } from "@material-ui/core";

import {
  Redirect,
  BrowserRouter as Router,
  Route,
  Switch,
  withRouter,
  useLocation,
  useHistory,
} from "react-router-dom";

import Register from "./components/Register";
import SignIn from "./components/SignIn";
import { RoutesIndex } from "./routes/RoutesIndex";
import { NotFound } from "./pages/NotFound";
import { HomePage } from "./pages/HomePage";
import { UserContext } from "./context/userContext";
import { TempContext } from "./context/tempContext";

import jwt_decode from "jwt-decode";
import { withCookies, useCookies, Cookies } from "react-cookie";
import { isEmpty } from "underscore";
import API from "./services/api";
import { SignContext } from "./context/signContext";
import { useSnackbar } from "notistack";

import PlayArrowIcon from "@material-ui/icons/PlayArrow";
import StopIcon from "@material-ui/icons/Stop";

const useStyles = makeStyles((theme) => ({
  content: {
    flexGrow: 1,
    padding: theme.spacing(3),
  },
  signButton: {
    "& > *": {
      margin: theme.spacing(3),
    },
    position: "fixed",
    bottom: 0,
    right: 0,
  },
}));

function App() {
  const classes = useStyles();
  const [loggedIn, setLoggedIn] = useState();
  const [cookies, setCookies] = useCookies(["userToken", "remember"]);
  const [userState, setUserState] = useContext(UserContext);
  const [signState, setSignState] = useContext(SignContext);
  const [tempState, setTempState] = useContext(TempContext);
  const history = useHistory();
  const { enqueueSnackbar } = useSnackbar();

  const getUserSign = async () => await API.sign.getUserSign(userState._id);

  useEffect(() => {
    if (userState.role === "User") {
      console.log("sdf");
      getUserSign().then(({ data: { sign } }) => {
        for (let i = 0; i < sign.length; i++) {
          if (sign[i].date_end === null) {
            setSignState(sign[i]);
          }
        }
      });
    }
  }, [userState]);

  useEffect(() => {
    if (!isEmpty(cookies)) {
      if (!isEmpty(cookies.userToken)) {
        const { exp, user } = jwt_decode(cookies.userToken);
        setUserState(user);
        setLoggedIn(true);
      } else {
        setLoggedIn(false);
      }
    } else {
      setLoggedIn(false);
    }
  }, [cookies]);

  const startSign = async () => {
    const {
      status,
      data: { sign: signData },
    } = await API.sign.startSign({
      user: userState._id,
    });

    if (status === 200) {
      enqueueSnackbar("Has comenzado a registrar tu jornada", {
        variant: "success",
        anchorOrigin: {
          vertical: "bottom",
          horizontal: "center",
        },

        autoHideDuration: 1500,
      });
      setSignState(signData);
    }
  };

  const stopSign = async () => {
    const now = new Date();
    const start_date = new Date(signState.date_start);
    const currentTime = new Date(
      new Date().getTime() + (now.getTime() - start_date.getTime() - 1000)
    );

    const { status } = await API.sign.endSign({
      date_end: currentTime,
      total_hours: `${
        tempState.hours.toString().length > 1
          ? `0${tempState.hours}`
          : tempState.hours
      }:${
        tempState.minutes.toString().length === 1
          ? `0${tempState.minutes}`
          : tempState.minutes
      }:${
        tempState.seconds.toString().length === 1
          ? `0${tempState.seconds}`
          : tempState.seconds
      }`,
      id: signState._id,
    });

    if (status === 200) {
      enqueueSnackbar(
        "Has terminado de registrar tu jornada laboral. Hasta la próxima!",
        {
          variant: "success",
          anchorOrigin: {
            vertical: "bottom",
            horizontal: "center",
          },

          autoHideDuration: 1500,
        }
      );
      setSignState(null);
      setTempState({
        hours: 0,
        minutes: 0,
        seconds: 0,
      });
    }
  };

  return (
    <Fragment>
      <Router>
        <Switch>
          <Route exact path="/page-not-found" component={NotFound} />
          <Route exact path={_STATIC.LOGIN}>
            {!loggedIn ? (
              <SignIn setLoggedIn={setLoggedIn} />
            ) : (
              <Redirect to={_STATIC.SH_HOME} />
            )}
          </Route>
          <Route exact path={_STATIC.REGISTER}>
            <Register />
          </Route>
          <Route path={_STATIC.SH_HOME}>
            {!loggedIn ? <Redirect to={_STATIC.LOGIN} /> : <RoutesIndex />}
          </Route>
        </Switch>
      </Router>
      {loggedIn && userState.role !== "Admin" && (
        <div className={classes.signButton}>
          <Fab
            color={
              signState === null || signState.date_start === null
                ? "primary"
                : "secondary"
            }
            onClick={() => {
              signState === null || signState.date_start === null
                ? startSign()
                : stopSign();
            }}
          >
            {signState === null || signState.date_start === null ? (
              <PlayArrowIcon />
            ) : (
              <StopIcon />
            )}
          </Fab>
        </div>
      )}
    </Fragment>
  );
}

export default withCookies(App);
