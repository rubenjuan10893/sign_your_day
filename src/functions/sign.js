export const startSign = async () => {
    const {
      status,
      data: { sign: signData },
    } = await API.sign.startSign({
      user: userState._id,
    });

    if (status === 200) {
      enqueueSnackbar("Has comenzado a registrar tu jornada", {
        variant: "success",
        anchorOrigin: {
          vertical: "bottom",
          horizontal: "center",
        },

        autoHideDuration: 1500,
      });
      setSignState(signData);
    }
  };

export const stopSign = async () => {
    const now = new Date();
    const start_date = new Date(signState.date_start);
    const currentTime = new Date(
      new Date().getTime() + (now.getTime() - start_date.getTime() - 1000)
    );

    const { status } = await API.sign.endSign({
      date_end: currentTime,
      total_hours: `${
        tempState.hours.toString().length > 1
          ? `0${tempState.hours}`
          : tempState.hours
      }:${
        tempState.minutes.toString().length === 1
          ? `0${tempState.minutes}`
          : tempState.minutes
      }:${
        tempState.seconds.toString().length === 1
          ? `0${tempState.seconds}`
          : tempState.seconds
      }`,
      id: signState._id,
    });