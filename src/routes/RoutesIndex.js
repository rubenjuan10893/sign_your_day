import { Fragment, useState } from "react";
import { Redirect, Route, Switch, useLocation } from "react-router-dom";
import clsx from "clsx";
import { Menu } from "../components/menu/Menu";
import { routesData } from "../data/routesData";
import { HomePage } from "../pages/HomePage";
import { _STATIC } from "./routesConst";
import { indexStyles } from "../styles";

export const RoutesIndex = () => {
  const classes = indexStyles();
  const [found, setFound] = useState(false);

  return (
    <Fragment>
      <Menu />
      <main className={clsx(classes.content, classes.mainContent)}>
        <Switch>
          <Route exact path={_STATIC.SH_HOME}>
            <Redirect to={_STATIC.HOME} />
          </Route>

          {routesData.map((routeProp, index) => (
            <Route key={index} exact {...routeProp} />
          ))}

          <Route path="*">
            <Redirect to="/page-not-found" />
          </Route>
        </Switch>
      </main>
    </Fragment>
  );
};
