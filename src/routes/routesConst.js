export const _STATIC = {
  SH_HOME: "/",
  HOME: "/home",
  LOGIN: "/login",
  REGISTER: "/register",
  PROFILE: "/profile",
};

export const _PAGE = {
  USERS: "/users",
  TASKS: "/tasks",
  SIGNING: "/signing",
  ABSENCE: "/absence",
};
