import { makeStyles } from "@material-ui/core";

export const generalStyles = makeStyles((theme) => ({
  rootAlert: {
    width: "100%",
    "& > * + *": {
      marginTop: theme.spacing(2),
    },
  },
  link: {
    "&:hover": {
      color: "rgba(0, 0, 0, 0.87)",
    },
  },
  alertContent: {
    display: "flex",
    justifyContent: "center",
    padding: theme.spacing(2),
  },
  loadingContainer: {
    width: "100%",
    display: "flex",
    justifyContent: "center",
  },
  loadingCircularSpinner: {
    color: "#1f334e",
    textAlign: "center",
  },
  rootTitleContainer: {
    width: "100%",
    marginBottom: 16,
    textAlign: "center",
    display: "flex",
    justifyContent: "center",
    alignItems: "flex-start",
    flexDirection: "column",
  },
  titleContainer: {
    display: "flex",
    justifyContent: "flex-start",
    alignItems: "center",
    width: "100%",
  },
  title: {
    width: "100%",
    textAlign: "center",
  },
  divider: {
    width: "100%",
  },
}));
