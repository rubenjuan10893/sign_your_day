import { makeStyles } from "@material-ui/core";
import { green, grey } from "@material-ui/core/colors";

export const buttonStyles = makeStyles((theme) => ({
  buttonRefreshContainer: {
    width: 55,
    height: 55,
    borderRadius: "10px 10px 10px 10px",
    display: "flex",
  },
  button: {
    display: "flex",
    minWidth: 53,
    width: "100%",
    borderRadius: "10px 10px 10px 10px",
  },
  iconRefresh: {
    color: "#1f334e",
  },
  buttonSuccess: {
    backgroundColor: green[500],
    "&:hover": {
      backgroundColor: green[700],
    },
  },
  disabledButton: {
    backgroundColor: grey[500],
    color: grey[200],
  },
  buttonProgress: {
    color: green[500],
    position: "absolute",
    top: "50%",
    left: "50%",
    marginTop: -12,
    marginLeft: -12,
  },
  fabProgress: {
    color: green[500],
    position: "absolute",
    top: -6,
    left: -6,
    zIndex: 1,
  },
  marginButtonSuccess: {
    margin: theme.spacing(1, 9, 1, 1),
  },
}));
