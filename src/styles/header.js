import { makeStyles } from "@material-ui/core";

const drawerWidth = 190;

export const headerStyles = makeStyles((theme) => ({
  appBar: {
    width: `calc(100% - ${drawerWidth}px)`,
    [theme.breakpoints.up("sm")]: {
      marginLeft: drawerWidth,
    },
  },
  toolbar: {
    ...theme.mixins.toolbar,
    display: "flex",
    justifyContent: "space-between",
    alignItems: "center",
    boxShadow: "0 0 10px 0",
    backgroundColor: "#f2f2f2",
  },
  // estilos para el contenedor de la foto de perfil y la bienvenida
  userInfo: {
    display: "flex",
    width: 230,
    justifyContent: "space-around",
    alignItems: "center",
    color: "rgba(0, 0, 0, 0.87)",
  },
  tempContainer: {
    display: "flex",
    alignItems: "center",
    width: "75%",
    justifyContent: "space-around",
  },
  userTitle: {
    fontSize: "1rem",
  },
}));
