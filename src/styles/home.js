import { makeStyles } from "@material-ui/core";

export const homeStyles = makeStyles((theme) => ({
  // Contenedor principal de la cuadrícula
  rootContainer: {
    display: "flex",
    flexWrap: "wrap",
    justifyContent: "center",
  },
  container: {
    display: "grid",
    gridAutoFlow: "row",
  },
  clockTitle: {
    color: theme.palette.grey[400],
    textAlign: "center",
  },
  createButton: {},
  clockButton: {
    width: "max-content",
    textTransform: "none",
    padding: theme.spacing(2) + "px !important",
    margin: theme.spacing(3, 1, 1, 1),
  },
  firstItemUser: {
    display: "flex",
    flexDirection: "column",
    justifyContent: "center",
    alignItems: "center",
  },
  firstItemAdmin: {
    background:
      "linear-gradient(to bottom right, rgba(0,153,255,1) 0%, rgba(31,51,78,1) 85%) !important",
  },
  // Estilos para cada elemento de la cuadrícula
  item: {
    display: "flex",
    flexDirection: "column",
    position: "relative",
    borderRadius: "10px",
    margin: theme.spacing(2),
    background: "#fff",
    color: "rgba(0, 0, 0, 0.87)",
    minWidth: 300,
    paddingBottom: theme.spacing(1),
  },
  lastItem: {
    height: "95.5%",
  },
  item1: {
    borderTop: `3px solid ${theme.palette.info.main}`,
  },
  item2: {
    borderTop: `3px solid ${theme.palette.info.dark}`,
  },
  item3: {
    borderTop: `3px solid ${theme.palette.primary.light}`,
  },
  item4: {
    borderTop: `3px solid ${theme.palette.primary.dark}`,
  },
  // Estilos para el primer elemento de la cuadrícula en la página de inicio
  // AL que aplicaremos un difuminado de color de fondo
  dividerTitle: {
    width: "100%",
  },
  itemTitle: {
    fontSize: 15,
    width: "100%",
    textAlign: "center",
    padding: "15px 0",
    top: 0,
  },
  adminItemTitle: {
    color: "#fff",
  },
  descriptionContainer: {
    marginTop: 8,
    height: "100%",
  },
  itemDescription: {
    margin: theme.spacing(0, 2),
    textAlign: "justify",
    display: "flex",
    flexDirection: "column",
    justifyContent: "space-between",
    height: "100%",
  },
  adminItemDescription: {
    margin: theme.spacing(2),
    textAlign: "justify",
    color: "#fff",
  },
  showMoreAbsencesContainer: {
    display: "flex",
    alignItems: "center",
    justifyContent: "space-around",
    padding: theme.spacing(2),
    textAlign: "center",
  },
  calendarContainer: {
    width: 50,
    height: 50,
    display: "flex",
    flexDirection: "column",
    borderRadius: "30px 30px 5px 5px",
  },
  calendarHeader: {
    height: 25,
    backgroundColor: "#d93453",
    borderRadius: "15px 15px 0 0",
    textAlign: "center",
    color: "#fff",
    fontSize: 11,
  },
  calendarBody: {
    backgroundColor: "#f3f5f7",
    borderRadius: "0 0 20px 20px",
    height: "100%",
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    fontSize: 20,
  },
}));
