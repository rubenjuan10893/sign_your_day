import { makeStyles } from "@material-ui/core";

export const tableStyles = makeStyles((theme) => ({
  cardRoot: {
    minWidth: 275,
  },
  cardContent: {
    padding: 16 + "px !important",
  },
  root: {
    width: "100%",
  },
  container: {
    maxHeight: 600,
  },
  dayContainer: {
    display: "flex",
    flexDirection: "column",
  },
  stateAccepted: {
    opacity: 1 + " !important",
    backgroundColor: theme.palette.success.main,
  },
  stateDeclined: {
    opacity: 1 + " !important",
    backgroundColor: theme.palette.error.main,
  },
  statePending: {
    opacity: 1 + " !important",
    backgroundColor: theme.palette.warning.main,
  },
  selectContainer: {
    display: "flex",
    justifyContent: "center",
  },
  selectFormControl: {
    minWidth: 120,
    width: "100%",
  },
  loadingSpinner: {
    width: 25 + "px !important",
    height: 25 + "px !important",
  },
  firstItemInfo: {
    marginRight: 4,
  },
  itemInfo: {
    marginRight: 4,
    marginLeft: 4,
  },
}));
