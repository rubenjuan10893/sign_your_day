import { makeStyles } from "@material-ui/core";

const drawerWidth = 190;

export const indexStyles = makeStyles((theme) => ({
  mainContent: {
    position: "relative",
    top: 65,
    display: "flex",
    flexDirection: "column",
  },
  content: {
    flexGrow: 1,
    padding: theme.spacing(2, 2, 2, 0),
    transition: theme.transitions.create("margin", {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
    marginLeft: drawerWidth + 25,
  },
  rootIndexContainer: {
    justifyContent: "center",
    minWidth: 362,
  },
}));
