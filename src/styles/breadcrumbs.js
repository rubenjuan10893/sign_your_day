import { fade, makeStyles } from "@material-ui/core";

export const breadcrumbStyles = makeStyles((theme) => ({
  breadcrumbsContainer: {
    padding: "16px 8px",
    margin: 4,
    backgroundColor: fade(theme.palette.grey[500], 0.15),
  },
  link: {
    color: theme.palette.text.primary,
    "&:hover": {
      textDecoration: "underline",
      cursor: "pointer",
    },
  },
  linkActive: {
    color: theme.palette.text.primary,
  },
}));
