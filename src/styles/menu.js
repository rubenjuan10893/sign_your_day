import { makeStyles } from "@material-ui/core";

const drawerWidth = 190;

export const menuStyles = makeStyles((theme) => ({
  root: {
    display: "flex",
  },
  title: {
    fontFamily: "Pacifico, cursive",
    fontSize: "1.5rem",
  },
  drawer: {
    [theme.breakpoints.up("sm")]: {
      width: drawerWidth,
      flexShrink: 0,
    },
  },
  menuButton: {
    marginRight: theme.spacing(2),
    [theme.breakpoints.up("sm")]: {
      display: "none",
    },
  },
  // necessary for content to be below app bar
  toolbar: {
    ...theme.mixins.toolbar,
    display: "flex",
    flexDirection: "column",
    justifyContent: "center",
    alignItems: "center",
    padding: "1rem",
  },
  drawerPaper: {
    width: drawerWidth,
    backgroundColor: "#1f334e",
    color: "#f2f2f2",
  },
  menuIcon: {
    color: "#f2f2f2",
  },
  link: {
    color: "white",
    textDecoration: "none",
  },
  item: {
    "&:hover": {
      backgroundColor: "#152235",
    },
  },
}));
