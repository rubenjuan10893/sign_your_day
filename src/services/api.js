import { create } from "apisauce";

const api_dev = `http://localhost:5000`;

const app = create({
  baseURL: api_dev,
});

const users = {
  getUsers: () => app.get(`/users`),
  getUser: (id) => app.get(`/users/${id}`),
  createUser: (user) => app.post(`/users/create`, user),
  updateUserGet: (id) => app.get(`/users/${id}/update`),
  updateUserPost: (id, user) => app.post(`/users/${id}/update`, user),
};

const sign = {
  getAllSign: () => app.get(`/sign/all`),
  getUserSign: (id, per_page = 25, page = 0) =>
    app.get(`/sign?id=${id}&per_page=${per_page}&page=${page}`),
  getTotalHours: (id) => app.get(`/sign/total-hours/${id}`),
  updateSignUser: (id_user, updatedSign) =>
    app.post(`/sign/${id_user}/update`, updatedSign),
  startSign: (sign) => app.post(`/sign/start`, sign),
  endSign: (sign) => app.post(`/sign/end`, sign),
  continueSign: (sign) => app.post(`/sign/continue`, sign),
};

const absence = {
  createAbsence: (absence) => app.post(`/absence/create`, absence),
  getUserAbsence: (id, per_page = 25, page = 0) =>
    app.get(`/absence/${id}?per_page=${per_page}&page=${page}`),
  getAbsenceList: (per_page = 25, page = 0) =>
    app.get(`/absence?per_page=${per_page}&page=${page}`),
  updateUserAbsence: (id, absence) =>
    app.post(`/absence/${id}/update`, absence),
};

const task = {
  getUserTask: (user_id) => app.get(`/task/${user_id}`),
  getTaskList: () => app.get(`/task`),
  createTask: (task) => app.post(`/task/create`, task),
  updateTask: (id, task) => app.post(`/task/${id}/update`, task),
};

const login = {
  logIn: (userCredentials) => app.post(`/login`, userCredentials),
};

const API = {
  users,
  sign,
  absence,
  task,
  login,
};

export default API;
