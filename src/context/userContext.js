import { createContext, useState } from "react";

export const UserProvider = ({ children }) => {
  const [userState, setUserState] = useState({});

  return (
    <UserContext.Provider value={[userState, setUserState]}>
      {children}
    </UserContext.Provider>
  );
};

export const UserContext = createContext();
