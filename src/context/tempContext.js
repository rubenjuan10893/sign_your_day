import { createContext, useState } from "react";

export const TempProvider = ({ children }) => {
  const [tempState, settempState] = useState({
    hours: 0,
    minutes: 0,
    seconds: 0,
  });

  return (
    <TempContext.Provider value={[tempState, settempState]}>
      {children}
    </TempContext.Provider>
  );
};

export const TempContext = createContext();
