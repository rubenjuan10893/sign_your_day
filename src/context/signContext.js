import { createContext, useState } from "react";

export const SignProvider = ({ children }) => {
  const [signState, setSignState] = useState(null);

  return (
    <SignContext.Provider value={[signState, setSignState]}>
      {children}
    </SignContext.Provider>
  );
};

export const SignContext = createContext();
