import React, { Fragment, useContext, useEffect, useState } from "react";
import clsx from "clsx";
import { makeStyles } from "@material-ui/core/styles";
import Paper from "@material-ui/core/Paper";

import { UserContext } from "../context/userContext";

import { Helmet } from "react-helmet";
import Table from "../components/sign/Table";

import API from "../services/api";
import {
  Button,
  Card,
  CardContent,
  CircularProgress,
  Divider,
  Grid,
  Typography,
} from "@material-ui/core";
import { CustomCard } from "../components/customComponents/CustomCard";
import { tableStyles } from "../styles/table";
import { buttonStyles } from "../styles/button";
import CachedIcon from "@material-ui/icons/Cached";
import { generalStyles } from "../styles/general";

const columnsUser = [
  { id: "date_start", align: "left", label: "Día", minWidth: "15%" },
  {
    id: "estimated_hours",
    label: "Horas Estimadas",
    minWidth: "15%",
    align: "left",
  },
  {
    id: "total_hours",
    label: "Total Horas",
    minWidth: "15%",
    align: "left",
  },
  {
    id: "state",
    label: "Estado Fichaje",
    minWidth: "14%",
    align: "center",
  },
];

const columnsAdmin = [
  { id: "employee_name", label: "Nombre Empleado", minWidth: "16%" },
  { id: "email", label: "Email", minWidth: "15%", align: "left" },
  ...columnsUser,
];

export const SigningPage = () => {
  const classes = tableStyles();
  const buttonClasses = buttonStyles();
  const generalClasses = generalStyles();
  const [userState, setUserState] = useContext(UserContext);
  const [signState, setSignState] = useState({ sign: [], count: 0 });
  const [loading, setLoading] = useState(true);
  const [time, setTime] = useState({});

  const getUserSign = async () => await API.sign.getUserSign(userState._id);
  const getTotalHours = async () => await API.sign.getTotalHours(userState._id);
  const getAllSign = async () => await API.sign.getAllSign();

  const refreshData = () => {
    setSignState({ sign: [], count: 0 });
    setLoading(true);
    setTimeout(() => {
      if (userState.role === "User") {
        getUserSign().then(
          ({ status: statusUserSign, data: { sign, sign_count } }) => {
            getTotalHours().then(
              ({ data: { total_hours, total_minutes, total_seconds } }) => {
                setTime({
                  totalHours: total_hours,
                  totalMinutes: total_minutes,
                  totalSeconds: total_seconds,
                });
                setSignState({
                  status: statusUserSign,
                  sign: [...sign],
                  count: sign_count,
                });
              }
            );
          }
        );
      } else if (userState.role === "Admin") {
        getAllSign()
          .then(({ status, data: { signs, signs_count } }) => {
            console.log(signs_count);
            setSignState({
              status: status,
              sign: [...signs],
              count: signs_count,
            });
          })
          .catch((err) => console.log(err));
      }
      setLoading(false);
    }, 500);
  };

  useEffect(() => {
    refreshData();
  }, []);

  return (
    <Fragment>
      <Helmet title="Fichajes | SignYourDay" />
      <div className={generalClasses.rootTitleContainer}>
        <div className={generalClasses.titleContainer}>
          <div className={buttonClasses.buttonRefreshContainer}>
            <Button className={buttonClasses.button} onClick={refreshData}>
              <CachedIcon
                fontSize="large"
                color="primary"
                classes={{ colorPrimary: buttonClasses.iconRefresh }}
              />
            </Button>
          </div>
          <div className={generalClasses.title}>
            <Typography variant="h4" component="h3">
              {userState.role === "Admin" ? (
                <span>Fichajes diarios de los usuarios</span>
              ) : (
                <span>Estos son tus fichajes {userState.name}</span>
              )}
            </Typography>
          </div>
        </div>
        <Divider className={generalClasses.divider} />
      </div>

      {!loading && signState.sign.length > 0 ? (
        <Fragment>
          {userState.role === "User" && (
            <Grid container>
              <Grid item xs={12} style={{ marginBottom: 16, display: "flex" }}>
                <Grid item md={4}>
                  <Card className={classes.cardRoot}>
                    <CardContent className={classes.cardContent}>
                      <CustomCard data={userState} time={time} />
                    </CardContent>
                  </Card>
                </Grid>
              </Grid>
            </Grid>
          )}
          <Paper className={classes.root}>
            <Table
              columnsAdmin={columnsAdmin}
              columnsUser={columnsUser}
              userState={userState}
              setUserState={setUserState}
              signState={signState}
              setSignState={signState}
              classes={classes}
            />
          </Paper>
        </Fragment>
      ) : !loading && signState.sign.length === 0 ? (
        <div className={generalClasses.loadingContainer}>
          <CircularProgress className={generalClasses.loadingCircularSpinner} />
        </div>
      ) : (
        loading && signState.status === 404 && <span>No hay registros</span>
      )}
    </Fragment>
  );
};
