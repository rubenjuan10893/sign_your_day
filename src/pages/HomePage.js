import React, { useContext, useEffect, useState, Fragment } from "react";
import clsx from "clsx";
import API from "../services/api";
import {
  Avatar,
  Box,
  Divider,
  Fab,
  Link as MaterialLink,
  Grid,
  List,
  ListItem,
  ListItemAvatar,
  ListItemText,
  makeStyles,
  Typography,
  Dialog,
  DialogTitle,
  DialogContent,
  DialogContentText,
  Button,
  DialogActions,
  TextField,
  TextareaAutosize,
  CircularProgress,
  Chip,
  MenuItem,
  Select,
  FormControl,
  InputLabel,
} from "@material-ui/core";

import { UserContext } from "../context/userContext";
import { TempContext } from "../context/tempContext";

import PlayArrowIcon from "@material-ui/icons/PlayArrow";
import AddIcon from "@material-ui/icons/Add";
import StopIcon from "@material-ui/icons/Stop";

import { Link } from "react-router-dom";
import { SignContext } from "../context/signContext";
import { useSnackbar, withSnackbar } from "notistack";
import { Helmet } from "react-helmet";
import { homeStyles } from "../styles/home";

import { _PAGE } from "../routes/routesConst";
import { useForm } from "../hooks/useForm";
import { MONTHS } from "../constants/constants";
import { generalStyles } from "../styles/general";
import { tableStyles } from "../styles/table";
import { isEmpty } from "underscore";
import { indexStyles } from "../styles";

const useStyles = makeStyles((theme) => ({
  saveButton: {
    backgroundColor: theme.palette.success.main,
    "&:hover": {
      backgroundColor: theme.palette.success.light,
    },
  },
  taskTitleContainer: {
    display: "flex",
    justifyContent: "space-between",
  },
}));

const upcomingHolidays = [
  {
    date: "08/01/2021",
    title: "My Birthday",
    description: "El día de mi cumpleaños!",
  },
  {
    date: "09/24/2021",
    title: "My Girlfriend's Birthday",
    description: "El día del cumpleaños de mi chica!",
  },
];

const taskProjects = [
  "Bookmeeting Own Control Panel Project",
  "DAW Final Project",
];

const HomePage = () => {
  const homeClasses = homeStyles();
  const ownClasses = useStyles();
  const generalClasses = generalStyles();
  const tableClasses = tableStyles();
  const indexClasses = indexStyles();

  const [formValues, handleInputChange] = useForm();

  const [userState, setUserState] = useContext(UserContext);
  const [signState, setSignState] = useContext(SignContext);
  const [usersAbsenceState, setUsersAbsenceState] = useState({
    absences: [],
    count: 0,
  });
  const [usersTaskState, setUsersTaskState] = useState({ tasks: [], count: 0 });
  const [openTaskDialog, setOpenTaskDialog] = useState(false);
  const [selectTaskProject, setSelectTaskProject] = useState("");
  const [openAbsenceDialog, setOpenAbsenceDialog] = useState(false);
  const [loading, setLoading] = useState(true);
  const [temp, setTemp] = useContext(TempContext);
  const { enqueueSnackbar } = useSnackbar();

  const getUserAbsence = async () => {
    const {
      status,
      data: { absences, count_absences },
    } = await API.absence.getUserAbsence(userState._id);

    if (status === 200) {
      setUsersAbsenceState({ absences: [...absences], count: count_absences });
    }
  };

  const getAbsenceList = async () => {
    const {
      status,
      data: { absences, count_absences },
    } = await API.absence.getAbsenceList();

    if (status === 200) {
      setUsersAbsenceState({
        absences: [...absences],
        count: count_absences,
      });
    }
  };

  const getUserTask = async () => {
    const {
      status,
      data: { tasks, count_tasks },
    } = await API.task.getUserTask(userState._id);

    if (status === 200) {
      setUsersTaskState({ tasks: [...tasks], count: count_tasks });
    }
  };

  const getTaskList = async () => {
    const {
      status,
      data: { tasks, count_tasks },
    } = await API.task.getTaskList();

    if (status === 200) {
      setUsersTaskState({
        tasks: [...tasks],
        count: count_tasks,
      });
    }
  };

  useEffect(() => {
    if (userState.role === "User") {
      getUserAbsence();
      getUserTask();
    } else if (userState.role === "Admin") {
      getAbsenceList();
      getTaskList();
    }
  }, [userState]);

  useEffect(() => {
    !isEmpty(usersTaskState) &&
      !isEmpty(usersAbsenceState) &&
      setLoading(false);
  }, [usersTaskState, usersAbsenceState]);

  const handleOpenAbsenceDialog = () => {
    setOpenAbsenceDialog(true);
  };

  const handleCloseAbsenceDialog = () => {
    setOpenAbsenceDialog(false);
  };

  const handleOpenTaskDialog = () => {
    setOpenTaskDialog(true);
  };

  const handleCloseTaskDialog = () => {
    setOpenTaskDialog(false);
  };

  const saveAbsence = async () => {
    const newAbsence = {
      ...formValues,
      user: userState._id,
    };

    const {
      status,
      data: { error },
    } = await API.absence.createAbsence(newAbsence);

    if (status === 200) {
      enqueueSnackbar("Se ha creado correctamente la Ausencia", {
        variant: "success",
        anchorOrigin: {
          vertical: "bottom",
          horizontal: "center",
        },

        autoHideDuration: 1500,
      });
    } else if (status === 400) {
      enqueueSnackbar(`No ha sido posible crear la Ausencia ${error.message}`, {
        variant: "error",
        anchorOrigin: {
          vertical: "bottom",
          horizontal: "center",
        },

        autoHideDuration: 1500,
      });
    }

    handleCloseAbsenceDialog();
  };

  const saveTask = async () => {
    const newTask = {
      ...formValues,
      user: userState._id,
      project: selectTaskProject,
    };

    const {
      status,
      data: { error },
    } = await API.task.createTask(newTask);

    if (status === 200) {
      enqueueSnackbar("Se ha creado correctamente la TAREA", {
        variant: "success",
        anchorOrigin: {
          vertical: "bottom",
          horizontal: "center",
        },

        autoHideDuration: 1500,
      });
    } else if (status === 400) {
      enqueueSnackbar(`No ha sido posible crear la TAREA ${error.message}`, {
        variant: "error",
        anchorOrigin: {
          vertical: "bottom",
          horizontal: "center",
        },

        autoHideDuration: 1500,
      });
    }

    handleCloseTaskDialog();
  };

  const startSign = async () => {
    const {
      status,
      data: { sign: signData },
    } = await API.sign.startSign({
      user: userState._id,
    });

    if (status === 200) {
      enqueueSnackbar("Has comenzado a registrar tu jornada", {
        variant: "success",
        anchorOrigin: {
          vertical: "bottom",
          horizontal: "center",
        },

        autoHideDuration: 1500,
      });
      setSignState(signData);
    }

    handleCloseAbsenceDialog();
  };

  const stopSign = async () => {
    const now = new Date();
    const start_date = new Date(signState.date_start);
    const currentTime = new Date(
      new Date().getTime() + (now.getTime() - start_date.getTime() - 1000)
    );

    const { status } = await API.sign.endSign({
      date_end: currentTime,
      total_hours: `${
        temp.hours.toString().length > 1 ? `0${temp.hours}` : temp.hours
      }:${
        temp.minutes.toString().length === 1 ? `0${temp.minutes}` : temp.minutes
      }:${
        temp.seconds.toString().length === 1 ? `0${temp.seconds}` : temp.seconds
      }`,
      id: signState._id,
    });

    if (status === 200) {
      enqueueSnackbar(
        "Has terminado de registrar tu jornada laboral. Hasta la próxima!",
        {
          variant: "success",
          anchorOrigin: {
            vertical: "bottom",
            horizontal: "center",
          },

          autoHideDuration: 1500,
        }
      );
      setSignState(null);
      setTemp({
        hours: 0,
        minutes: 0,
        seconds: 0,
      });
    }
  };

  return (
    <Fragment>
      <Helmet title="SignYourDay" />
      <Grid container className={indexClasses.rootIndexContainer}>
        <Grid item xs={8} className={homeClasses.rootContainer}>
          <Grid
            item
            xs={5}
            className={clsx(homeClasses.item, homeClasses.item1, {
              [homeClasses.firstItemAdmin]: userState.role === "Admin",
            })}
          >
            <Typography
              variant="caption"
              className={clsx(homeClasses.itemTitle, {
                [homeClasses.adminItemTitle]: userState.role === "Admin",
              })}
            >
              ¡BIENVENIDO, {userState.name.toUpperCase()}!
            </Typography>
            <Divider className={homeClasses.dividerTitle} />
            {userState.role === "User" ? (
              <div className={homeClasses.descriptionContainer}>
                <div
                  className={clsx(
                    homeClasses.itemDescription,
                    homeClasses.firstItemUser
                  )}
                >
                  <Typography
                    variant="h5"
                    className={clsx(homeClasses.clockTitle)}
                  >
                    {temp.hours}h{" "}
                    {temp.minutes.toString().length === 1
                      ? `0${temp.minutes}`
                      : temp.minutes}
                    m
                  </Typography>
                  <Fab
                    variant="extended"
                    color={
                      signState === null || signState.date_start === null
                        ? "primary"
                        : "secondary"
                    }
                    onClick={() => {
                      signState === null || signState.date_start === null
                        ? startSign()
                        : stopSign();
                    }}
                    className={homeClasses.clockButton}
                  >
                    {signState === null || signState.date_start === null ? (
                      <Fragment>
                        <PlayArrowIcon />
                        Check In
                      </Fragment>
                    ) : (
                      <Fragment>
                        <StopIcon />
                        Check Out
                      </Fragment>
                    )}
                  </Fab>
                </div>
              </div>
            ) : (
              <div
                style={{
                  height: "100%",
                  display: "flex",
                  alignItems: "center",
                }}
              >
                <div className={clsx(homeClasses.adminItemDescription)}>
                  Como Administrador, navega por la aplicación para gestionar
                  todos los usuarios registrados y poder aceptar o rechazar sus
                  fichajes, ausencias, etc!
                </div>
              </div>
            )}
          </Grid>
          <Grid
            item
            xs={5}
            className={clsx(homeClasses.item, homeClasses.item1)}
          >
            <Typography
              variant="caption"
              className={clsx(homeClasses.itemTitle)}
            >
              PROXIMAS AUSENCIAS
            </Typography>
            <Divider className={homeClasses.dividerTitle} />
            <div className={homeClasses.descriptionContainer}>
              {console.log(usersAbsenceState, loading)}
              <div className={homeClasses.itemDescription}>
                {!loading ? (
                  usersAbsenceState.absences.length > 0 ? (
                    <Fragment>
                      {usersAbsenceState.absences.map((absence, index) => (
                        <Fragment key={index}>
                          {index < 3 && (
                            <List disablePadding dense={true} key={index}>
                              {console.log(absence.user.avatar)}
                              <ListItem disableGutters alignItems="flex-start">
                                <ListItemAvatar>
                                  <Avatar src={absence.user.avatar} />
                                </ListItemAvatar>
                                <ListItemText
                                  primary={
                                    <div
                                      style={{
                                        display: "flex",
                                        justifyContent: "space-between",
                                      }}
                                    >
                                      <div>
                                        <Typography
                                          component="span"
                                          variant="body1"
                                        >
                                          {`${absence.user.name} ${absence.user.lastname}`}
                                        </Typography>
                                        <Typography
                                          component="span"
                                          variant="body2"
                                          color="textPrimary"
                                        >
                                          {` - ${new Date(
                                            absence.date_absence
                                          ).toLocaleDateString()} `}
                                        </Typography>
                                      </div>
                                      <Typography
                                        variant="caption"
                                        color="textSecondary"
                                        component="span"
                                      >
                                        <Chip
                                          color="primary"
                                          classes={{
                                            colorPrimary:
                                              absence.state === "Accepted"
                                                ? tableClasses.stateAccepted
                                                : tableClasses.stateDeclined,
                                          }}
                                          size="small"
                                          label={absence.state}
                                        />
                                      </Typography>
                                    </div>
                                  }
                                  secondary={
                                    <Fragment>
                                      <Typography
                                        component="span"
                                        variant="caption"
                                      >
                                        {`${absence.description}`}
                                      </Typography>
                                    </Fragment>
                                  }
                                />
                              </ListItem>
                            </List>
                          )}
                        </Fragment>
                      ))}
                      <Fragment>
                        <div className={homeClasses.showMoreAbsencesContainer}>
                          {usersAbsenceState.absences.length >= 3 && (
                            <div>
                              <MaterialLink component={Link} to={_PAGE.ABSENCE}>
                                Ver todas...
                              </MaterialLink>
                            </div>
                          )}
                          {userState.role !== "Admin" && (
                            <div>
                              <Fab
                                size="small"
                                color="primary"
                                onClick={handleOpenAbsenceDialog}
                              >
                                <AddIcon />
                              </Fab>
                            </div>
                          )}
                        </div>
                        <Dialog
                          open={openAbsenceDialog}
                          onClose={handleCloseAbsenceDialog}
                        >
                          <DialogTitle
                            style={{ cursor: "move", textAlign: "center" }}
                          >
                            Crear una nueva Ausencia
                          </DialogTitle>
                          <DialogContent>
                            <DialogContentText>
                              <Grid container spacing={3}>
                                <Grid item xs={12}>
                                  <Grid container spacing={2}>
                                    <Grid item xs={7}>
                                      <TextField
                                        required
                                        name="title"
                                        onChange={handleInputChange}
                                        fullWidth
                                        label="Titulo"
                                        variant="outlined"
                                      />
                                    </Grid>
                                    <Grid item xs={5}>
                                      <TextField
                                        fullWidth
                                        onChange={handleInputChange}
                                        name="date_absence"
                                        label="Fecha Ausencia"
                                        type="datetime-local"
                                        variant="outlined"
                                        InputLabelProps={{
                                          shrink: true,
                                        }}
                                      />
                                    </Grid>
                                    <Grid item xs={12}>
                                      <TextField
                                        label="Descripcion"
                                        name="description"
                                        onChange={handleInputChange}
                                        placeholder="Introduce una descripcion para la Ausencia"
                                        multiline
                                        fullWidth
                                        variant="outlined"
                                      />
                                    </Grid>
                                  </Grid>
                                </Grid>
                              </Grid>
                            </DialogContentText>
                          </DialogContent>
                          <DialogActions style={{ margin: "8px 16px" }}>
                            <Button
                              autoFocus
                              onClick={handleCloseAbsenceDialog}
                              color="secondary"
                              variant="contained"
                            >
                              Cancel
                            </Button>
                            <Button
                              color="primary"
                              classes={{
                                containedPrimary: ownClasses.saveButton,
                              }}
                              variant="contained"
                              onClick={saveAbsence}
                            >
                              Confirmar
                            </Button>
                          </DialogActions>
                        </Dialog>
                      </Fragment>
                    </Fragment>
                  ) : (
                    <span>{`${userState.name}, no tienes ausencias registradas!`}</span>
                  )
                ) : (
                  <div className={generalClasses.loadingContainer}>
                    <CircularProgress
                      className={generalClasses.loadingCircularSpinner}
                    />
                  </div>
                )}
              </div>
            </div>
          </Grid>
          <Grid
            item
            xs={5}
            className={clsx(homeClasses.item, homeClasses.item2)}
          >
            <Typography variant="caption" className={homeClasses.itemTitle}>
              PRÓXIMOS EVENTOS
            </Typography>
            <Divider className={homeClasses.dividerTitle} />
            <div className={homeClasses.descriptionContainer}>
              <div className={homeClasses.itemDescription}>
                <List disablePadding>
                  <ListItem disableGutters alignItems="flex-start">
                    <ListItemAvatar>
                      <div className={homeClasses.calendarContainer}>
                        <div className={homeClasses.calendarHeader}>
                          <span>
                            {MONTHS[new Date().getMonth() + 1].slice(0, 3)}
                          </span>
                        </div>
                        <div className={homeClasses.calendarBody}>24</div>
                      </div>
                    </ListItemAvatar>
                    <ListItemText
                      primary={
                        <Fragment>
                          <Typography component="span" variant="body2">
                            Presentación Proyecto FCT SignYourApp -
                          </Typography>
                          <Typography
                            component="span"
                            variant="body2"
                            color="textPrimary"
                          >
                            {" " + `24/05/2021`}
                          </Typography>
                        </Fragment>
                      }
                      secondary={
                        <Fragment>
                          <Typography component="span" variant="caption">
                            Día en el que habrá que presentar la aplicación
                            propuesta y desarrollada como proyecto final
                          </Typography>
                        </Fragment>
                      }
                    />
                  </ListItem>
                </List>
                <div className={homeClasses.showMoreAbsencesContainer}>
                  <div>
                    <MaterialLink component={Link} to={_PAGE.ABSENCE}>
                      Ver todos...
                    </MaterialLink>
                  </div>
                </div>
              </div>
            </div>
          </Grid>
          <Grid
            item
            xs={5}
            className={clsx(homeClasses.item, homeClasses.item3)}
          >
            <Typography variant="caption" className={homeClasses.itemTitle}>
              PRÓXIMOS FESTIVOS
            </Typography>
            <Divider className={homeClasses.dividerTitle} />
            <div className={homeClasses.descriptionContainer}>
              <div className={homeClasses.itemDescription}>
                <List disablePadding>
                  {upcomingHolidays.map((holiday) => (
                    <ListItem disableGutters alignItems="flex-start">
                      <ListItemAvatar>
                        <div className={homeClasses.calendarContainer}>
                          <div className={homeClasses.calendarHeader}>
                            <span>
                              {MONTHS[
                                new Date(holiday.date).getMonth() + 1
                              ].slice(0, 3)}
                            </span>
                          </div>
                          <div className={homeClasses.calendarBody}>
                            {new Date(holiday.date).getDate()}
                          </div>
                        </div>
                      </ListItemAvatar>
                      <ListItemText
                        primary={
                          <Fragment>
                            <Typography component="span" variant="body2">
                              {holiday.title}
                            </Typography>
                          </Fragment>
                        }
                        secondary={
                          <Fragment>
                            <Typography component="span" variant="caption">
                              {holiday.description}
                            </Typography>
                          </Fragment>
                        }
                      />
                    </ListItem>
                  ))}
                </List>
                <div className={homeClasses.showMoreAbsencesContainer}>
                  <div>
                    <MaterialLink component={Link} to={_PAGE.ABSENCE}>
                      Ver todos...
                    </MaterialLink>
                  </div>
                </div>
              </div>
            </div>
          </Grid>
        </Grid>
        <Grid
          item
          xs={8}
          sm={8}
          md={8}
          lg={4}
          className={homeClasses.rootContainer}
        >
          <Grid
            item
            xs={8}
            lg={8}
            xl={12}
            className={clsx(
              homeClasses.item,
              homeClasses.lastItem,
              homeClasses.item4
            )}
          >
            <Typography variant="caption" className={homeClasses.itemTitle}>
              TAREAS PENDIENTES
            </Typography>
            <Divider className={homeClasses.dividerTitle} />
            <div className={homeClasses.descriptionContainer}>
              <div className={homeClasses.itemDescription}>
                <div>
                  <List disablePadding>
                    {!loading &&
                      (usersTaskState.tasks.length > 0 ? (
                        usersTaskState.tasks.map((task, index) => {
                          const taskDate = new Date(task.createdAt);
                          return (
                            <Fragment>
                              {index < 3 && (
                                <ListItem
                                  disableGutters
                                  alignItems="flex-start"
                                >
                                  <ListItemAvatar>
                                    <Avatar>
                                      <span>
                                        {task.user.name
                                          .slice(0, 1)
                                          .toUpperCase()}
                                      </span>
                                    </Avatar>
                                  </ListItemAvatar>
                                  <ListItemText
                                    primary={
                                      <Fragment>
                                        <div
                                          className={
                                            ownClasses.taskTitleContainer
                                          }
                                        >
                                          <Typography
                                            component="span"
                                            variant="body1"
                                          >
                                            {`${
                                              userState.role === "Admin"
                                                ? "Desarrollador: "
                                                : "".trim()
                                            } ${task.user.name} ${
                                              task.user.lastname
                                            }`}
                                          </Typography>
                                          <Chip
                                            color="primary"
                                            classes={{
                                              colorPrimary:
                                                task.state === "Done"
                                                  ? tableClasses.stateAccepted
                                                  : tableClasses.statePending,
                                            }}
                                            size="small"
                                            label={task.state}
                                          />
                                        </div>
                                        <Typography
                                          component="span"
                                          variant="body2"
                                        >
                                          Tarea:{" "}
                                          <Typography
                                            component="span"
                                            variant="body2"
                                            color="textSecondary"
                                          >
                                            <span
                                              style={{
                                                textDecorationLine: "underline",
                                              }}
                                            >
                                              {task.title}
                                            </span>
                                          </Typography>
                                        </Typography>
                                      </Fragment>
                                    }
                                    secondary={
                                      <Fragment>
                                        <Typography
                                          component="span"
                                          variant="subtitle2"
                                        >
                                          {`${task.description}`}
                                          <br />
                                        </Typography>
                                        <Typography
                                          component="span"
                                          variant="caption"
                                        >
                                          <span style={{ fontStyle: "italic" }}>
                                            Fecha de Creación:{" "}
                                            {`${taskDate.toLocaleDateString()} - ${taskDate.getUTCHours()}:${taskDate.getUTCMinutes()}:${taskDate.getUTCSeconds()}`}
                                          </span>
                                          <br />
                                        </Typography>
                                        <Typography
                                          component="span"
                                          variant="caption"
                                        >
                                          <span style={{ fontStyle: "italic" }}>
                                            Proyecto Asignado: {task.project}
                                          </span>
                                        </Typography>
                                      </Fragment>
                                    }
                                  />
                                </ListItem>
                              )}
                            </Fragment>
                          );
                        })
                      ) : (
                        <div>No hay tareas registradas!</div>
                      ))}
                  </List>
                </div>

                <div className={homeClasses.showMoreAbsencesContainer}>
                  {usersTaskState.tasks.length >= 3 && (
                    <div>
                      <MaterialLink component={Link} to={_PAGE.ABSENCE}>
                        Ver todas...
                      </MaterialLink>
                    </div>
                  )}
                  {userState.role === "Admin" && (
                    <div>
                      <Fab
                        size="small"
                        color="primary"
                        onClick={handleOpenTaskDialog}
                      >
                        <AddIcon />
                      </Fab>
                    </div>
                  )}
                </div>
                <Dialog
                  open={openTaskDialog}
                  onClose={handleCloseTaskDialog}
                  maxWidth={false}
                >
                  <DialogTitle>Crear una nueva Ausencia</DialogTitle>
                  <DialogContent>
                    <DialogContentText>
                      <Grid container spacing={3}>
                        <Grid item xs={12}>
                          <Grid container spacing={2}>
                            <Grid item xs={7}>
                              <TextField
                                required
                                name="title"
                                onChange={handleInputChange}
                                fullWidth
                                label="Titulo"
                                variant="outlined"
                              />
                            </Grid>
                            <Grid item xs={5}>
                              <FormControl fullWidth variant="outlined">
                                <InputLabel id="select_label">
                                  Selecciona un Proyecto
                                </InputLabel>
                                <Select
                                  value={selectTaskProject}
                                  onChange={(e) =>
                                    setSelectTaskProject(e.target.value)
                                  }
                                  labelId="select_label"
                                  label="Selecciona un Proyecto"
                                >
                                  <MenuItem value="" disabled>
                                    ---
                                  </MenuItem>
                                  {taskProjects.map((project) => (
                                    <MenuItem
                                      key={taskProjects.indexOf(project)}
                                      value={project}
                                    >
                                      {project}
                                    </MenuItem>
                                  ))}
                                </Select>
                              </FormControl>
                            </Grid>
                            <Grid item xs={12}>
                              <TextField
                                label="Descripcion"
                                name="description"
                                onChange={handleInputChange}
                                placeholder="Introduce una descripcion para la Ausencia"
                                multiline
                                fullWidth
                                variant="outlined"
                              />
                            </Grid>
                          </Grid>
                        </Grid>
                      </Grid>
                    </DialogContentText>
                  </DialogContent>
                  <DialogActions style={{ margin: "8px 16px" }}>
                    <Button
                      autoFocus
                      onClick={handleCloseTaskDialog}
                      color="secondary"
                      variant="contained"
                    >
                      Cancel
                    </Button>
                    <Button
                      color="primary"
                      classes={{
                        containedPrimary: ownClasses.saveButton,
                      }}
                      variant="contained"
                      onClick={saveTask}
                    >
                      Confirmar
                    </Button>
                  </DialogActions>
                </Dialog>
              </div>
            </div>
          </Grid>
        </Grid>
      </Grid>
    </Fragment>
  );
};

export default HomePage;
