import React, { Fragment, useContext, useEffect, useState } from "react";
import { makeStyles } from "@material-ui/core/styles";
import Paper from "@material-ui/core/Paper";

import { UserContext } from "../context/userContext";

import { Helmet } from "react-helmet";
import Table from "../components/absence/Table";
import CachedIcon from "@material-ui/icons/Cached";

import API from "../services/api";
import {
  Button,
  CircularProgress,
  Divider,
  Typography,
} from "@material-ui/core";
import { buttonStyles } from "../styles/button";
import { generalStyles } from "../styles/general";

const columnsUser = [
  {
    id: "date_absence",
    align: "left",
    label: "Fecha Ausencia",
    minWidth: "15%",
  },
  {
    id: "title",
    label: "Título Ausencia",
    minWidth: "15%",
    align: "left",
  },
  {
    id: "description",
    label: "Descripcion Ausencia",
    minWidth: "15%",
    align: "left",
  },
  {
    id: "state",
    label: "Estado Ausencia",
    minWidth: "24%",
    align: "center",
  },
];

const columnsAdmin = [
  { id: "employee_name", label: "Nombre Empleado", minWidth: "16%" },
  { id: "email", label: "Email", minWidth: "15%", align: "left" },
  ...columnsUser,
];

const useStyles = makeStyles((theme) => ({
  cardRoot: {
    minWidth: 275,
  },
  root: {
    width: "100%",
  },
  container: {
    maxHeight: 600,
  },
  dayContainer: {
    display: "flex",
    flexDirection: "column",
  },
  stateAccepted: {
    opacity: 1 + " !important",
    backgroundColor: theme.palette.success.main,
  },
  stateDeclined: {
    opacity: 1 + " !important",
    backgroundColor: theme.palette.error.main,
  },
  statePending: {
    opacity: 1 + " !important",
    backgroundColor: theme.palette.warning.main,
  },
  selectContainer: {
    display: "flex",
    justifyContent: "center",
  },
  selectFormControl: {
    minWidth: 120,
    width: "100%",
  },
  loadingSpinner: {
    width: 25 + "px !important",
    height: 25 + "px !important",
  },
}));

export const AbsencePage = () => {
  const classes = useStyles();
  const buttonClasses = buttonStyles();
  const generalClasses = generalStyles();
  const [userState] = useContext(UserContext);
  const [absenceState, setAbsenceState] = useState({ absences: [], count: 0 });
  const [loading, setLoading] = useState(true);

  const getUserAbsence = async () =>
    await API.absence.getUserAbsence(userState._id);
  const getAbsenceList = async () => await API.absence.getAbsenceList();

  const refreshData = () => {
    setAbsenceState({ absences: [], count: 0 });
    setLoading(true);
    setTimeout(() => {
      if (userState.role === "User") {
        getUserAbsence().then(
          ({ status, data: { absences, count_absences } }) => {
            setAbsenceState({
              status: status,
              absences: [...absences],
              count: count_absences,
            });
          }
        );
      } else if (userState.role === "Admin") {
        getAbsenceList().then(
          ({ status, data: { absences, count_absences } }) => {
            setAbsenceState({
              status: status,
              absences: [...absences],
              count: count_absences,
            });
          }
        );
      }
      setLoading(false);
    }, 500);
  };

  useEffect(() => {
    refreshData();
  }, []);

  return (
    <Fragment>
      <Helmet title="Fichajes | SignYourDay" />
      <div className={generalClasses.rootTitleContainer}>
        <div className={generalClasses.titleContainer}>
          <div className={buttonClasses.buttonRefreshContainer}>
            <Button className={buttonClasses.button} onClick={refreshData}>
              <CachedIcon
                fontSize="large"
                color="primary"
                classes={{ colorPrimary: buttonClasses.iconRefresh }}
              />
            </Button>
          </div>
          <div className={generalClasses.title}>
            <Typography variant="h4">
              {userState.role === "Admin" ? (
                <span>Próximas ausencias de usuarios</span>
              ) : (
                <span>Estas son tus próximas ausencias {userState.name}</span>
              )}
            </Typography>
          </div>
        </div>
        <Divider className={generalClasses.divider} />
      </div>
      {!loading && absenceState.absences.length > 0 ? (
        <Paper className={classes.root}>
          <Table
            columnsAdmin={columnsAdmin}
            columnsUser={columnsUser}
            userState={userState}
            absenceState={absenceState}
            setAbsenceState={setAbsenceState}
            classes={classes}
            loading={loading}
          />
        </Paper>
      ) : !loading && absenceState.absences.length === 0 ? (
        <div className={generalClasses.loadingContainer}>
          <CircularProgress className={generalClasses.loadingCircularSpinner} />
        </div>
      ) : (
        loading &&
        absenceState.absences.length === 0 && <span>No hay registros</span>
      )}
    </Fragment>
  );
};
