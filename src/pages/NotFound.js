import React from "react";
import { Helmet } from "react-helmet";
import { Link } from "react-router-dom";
import "../styles/not_found.module.css";

export const NotFound = () => {
  return (
    <>
      <Helmet title="404 - Página no encontrada | SignYourDay" />
      <div className="notFound">
        <div className="codeArea">
          <h1>404</h1>
          <span style={{ color: "#777", fontStyle: "italic" }}>
            {"// "}Page not found!.
          </span>
          <span>
            <span style={{ color: "#d65562" }}>if</span>
            {"("}
            <span style={{ color: "#4ca8ef" }}>!</span>
            <span style={{ fontStyle: "italic", color: "#bdbdbd" }}>found</span>
            {")"}
            {"{"}
          </span>
          <span>
            <span style={{ paddingLeft: "15px", color: "#2796ec" }}>
              <i style={{ width: "10px", display: "inline-block" }}></i>throw
            </span>
            <span>
              (<span style={{ color: "#a6a61f" }}>{'"(╯°□°)╯︵ ┻━┻"'}</span>);
            </span>
            <span style={{ display: "block" }}>{"}"}</span>
            <span style={{ color: "#777", fontStyle: "italic" }}>
              {"// "}
              {/* <a href="/">Go home!</a> */}
              <Link to="/">Go home!</Link>
            </span>
          </span>
          <br />
        </div>
      </div>
    </>
  );
};
