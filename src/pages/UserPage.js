import React, { useContext, useEffect, useState } from "react";

import { UserContext } from "../context/userContext";
import Table from "../components/user/Table";
import { tableStyles } from "../styles/table";
import { generalStyles } from "../styles/general";
import API from "../services/api";
import { MONTHS } from "../constants/constants";
import { Fragment } from "react";
import {
  Button,
  CircularProgress,
  Divider,
  Paper,
  Typography,
} from "@material-ui/core";
import { buttonStyles } from "../styles/button";
import CachedIcon from "@material-ui/icons/Cached";
import { Alert } from "@material-ui/lab";

const columns = [
  {
    id: "employee_name",
    label: "Nombre Empleado",
    minWidth: "15%",
    align: "left",
  },
  {
    id: "email",
    label: "Email",
    minWidth: "25%",
    align: "center",
  },
  {
    id: "month",
    label: "Mes",
    minWidth: "25%",
    align: "center",
  },
  {
    id: "total_hours",
    label: "Horas Totales",
    minWith: "25%",
    align: "center",
  },
];

export const UserPage = () => {
  const classes = tableStyles();
  const generalClasses = generalStyles();
  const buttonClasses = buttonStyles();
  const [loading, setLoading] = useState();
  const [usersSignsState, setUsersSignsState] = useState({
    signs: [],
    count: 0,
  });

  const getUsers = async () => await API.users.getUsers();
  const getUserSign = async (user_id) => await API.sign.getUserSign(user_id);
  const getTotalHours = async (user_id) =>
    await API.sign.getTotalHours(user_id);

  const refreshData = () => {
    let arrAux = [];
    setUsersSignsState({ signs: [], count: 0 });

    setLoading(true);
    setTimeout(() => {
      getUsers().then(({ data: { users, count_users } }) => {
        for (const user of users) {
          user.role !== "Admin" &&
            getUserSign(user._id).then(
              ({ status: statusUserSign, data: { sign } }) => {
                getTotalHours(user._id).then(
                  ({ data: { total_hours, total_minutes, total_seconds } }) => {
                    if (
                      new Date(sign[0].date_start).getMonth() ===
                      new Date(sign[sign.length - 1].date_start).getMonth()
                    ) {
                      arrAux.push({
                        user: {
                          name: user.name,
                          email: user.email,
                        },
                        signMonth:
                          MONTHS[new Date(sign[0].date_start).getMonth() + 1],
                        time: `${
                          total_hours.toString().length === 1
                            ? `0${total_hours}`
                            : `${total_hours}`
                        }:${
                          total_minutes.toString().length === 1
                            ? `0${total_minutes}`
                            : `${total_minutes}`
                        }:${
                          total_seconds.toString().length === 1
                            ? `0${total_seconds}`
                            : `${total_seconds}`
                        }`,
                      });

                      setUsersSignsState({
                        status: statusUserSign,
                        signs: [...arrAux],
                        count: count_users,
                      });
                    }
                  }
                );
              }
            );
        }
      });
      setLoading(false);
    }, 500);
  };

  useEffect(() => {
    refreshData();
  }, []);

  return (
    <Fragment>
      <div className={generalClasses.rootTitleContainer}>
        <div className={generalClasses.titleContainer}>
          <div className={buttonClasses.buttonRefreshContainer}>
            <Button className={buttonClasses.button} onClick={refreshData}>
              <CachedIcon
                fontSize="large"
                color="primary"
                classes={{ colorPrimary: buttonClasses.iconRefresh }}
              />
            </Button>
          </div>
          <div className={generalClasses.title}>
            <Typography variant="h4">Horas Totales Usuarios</Typography>
          </div>
        </div>
        <Divider className={generalClasses.divider} />
      </div>
      {!loading && usersSignsState.signs.length > 0 ? (
        <Paper>
          <Table
            columns={columns}
            classes={classes}
            usersSignsState={usersSignsState}
          />
        </Paper>
      ) : !loading && usersSignsState.signs.length === 0 ? (
        <div className={generalClasses.loadingContainer}>
          <CircularProgress className={generalClasses.loadingCircularSpinner} />
        </div>
      ) : (
        loading &&
        usersSignsState.status === 404 && (
          <div className={generalClasses.rootAlert}>
            <Alert
              className={generalClasses.alertContent}
              variant="filled"
              severity="warning"
            >
              No hay Registros
            </Alert>
          </div>
        )
      )}
    </Fragment>
  );
};
