import {
  Button,
  CircularProgress,
  Divider,
  Paper,
  Typography,
} from "@material-ui/core";
import React, { useContext, useEffect, useState } from "react";
import { Fragment } from "react";
import Table from "../components/task/Table";
import { UserContext } from "../context/userContext";
import API from "../services/api";
import { buttonStyles } from "../styles/button";
import { generalStyles } from "../styles/general";
import { tableStyles } from "../styles/table";
import CachedIcon from "@material-ui/icons/Cached";
import { Alert } from "@material-ui/lab";

const columnsUser = [
  {
    id: "nom_project",
    label: "Nombre Proyecto",
    minWidth: "10%",
    align: "left",
  },
  {
    id: "nom_task",
    label: "Tarea",
    minWidth: "10%",
    align: "center",
  },
  {
    id: "description",
    label: "Descripción",
    minWidth: "10%",
    align: "center",
  },
  {
    id: "createdAt",
    label: "Fecha Creación",
    minWidth: "10%",
    align: "center",
  },
  {
    id: "state",
    label: "Estado Tarea",
    minWidth: "10%",
    align: "center",
  },
  {
    id: "status",
    label: "Tarea Apta",
    minWidth: "10%",
    align: "center",
  },
];

const columnsAdmin = [
  {
    id: "employee_name",
    label: "Nombre Empleado",
    minWidth: "10%",
    align: "center",
  },
  { id: "email", label: "Email", minWidth: "10%", align: "center" },
  ...columnsUser,
];

export const TaskPage = () => {
  const tableClasses = tableStyles();
  const generalClasses = generalStyles();
  const buttonClasses = buttonStyles();
  const [userState, setUserState] = useContext(UserContext);
  const [loading, setLoading] = useState(true);
  const [usersTasksState, setUsersTasksState] = useState({
    tasks: [],
    count: 0,
  });

  const getTaskList = async () => await API.task.getTaskList();
  const getUserTask = async () => await API.task.getUserTask(userState._id);

  const refreshData = () => {
    setUsersTasksState({
      tasks: [],
      count: 0,
    });
    setLoading(true);
    setTimeout(() => {
      if (userState.role === "User") {
        getUserTask().then(({ status, data: { tasks, count_tasks } }) => {
          setUsersTasksState({
            status: status,
            tasks: [...tasks],
            count: count_tasks,
          });
        });
      } else if (userState.role === "Admin") {
        getTaskList().then(({ status, data: { tasks, count_tasks } }) => {
          setUsersTasksState({
            status: status,
            tasks: [...tasks],
            count: count_tasks,
          });
        });
      }
      setLoading(false);
    }, 500);
  };

  useEffect(() => {
    refreshData();
  }, []);

  return (
    <Fragment>
      <div className={generalClasses.rootTitleContainer}>
        <div className={generalClasses.titleContainer}>
          <div className={buttonClasses.buttonRefreshContainer}>
            <Button className={buttonClasses.button} onClick={refreshData}>
              <CachedIcon
                fontSize="large"
                color="primary"
                classes={{ colorPrimary: buttonClasses.iconRefresh }}
              />
            </Button>
          </div>
          <div className={generalClasses.title}>
            <Typography variant="h4">
              {userState.role === "Admin" ? (
                <span>Resumen de tareas de los usuarios</span>
              ) : (
                <span>Estas son tus tareas {userState.name}</span>
              )}
            </Typography>
          </div>
        </div>
        <Divider className={generalClasses.divider} />
      </div>
      {!loading && usersTasksState.tasks.length > 0 ? (
        <Paper>
          <Table
            columns={userState.role === "Admin" ? columnsAdmin : columnsUser}
            classes={tableClasses}
            usersTasksState={usersTasksState}
            userState={userState}
          />
        </Paper>
      ) : !loading && usersTasksState.tasks.length === 0 ? (
        <div className={generalClasses.loadingContainer}>
          <CircularProgress className={generalClasses.loadingCircularSpinner} />
        </div>
      ) : (
        loading &&
        usersTasksState.status === 404 && (
          <div className={generalClasses.rootAlert}>
            <Alert
              className={generalClasses.alertContent}
              variant="filled"
              severity="warning"
            >
              No hay Registros
            </Alert>
          </div>
        )
      )}
    </Fragment>
  );
};
