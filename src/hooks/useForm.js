import { useEffect, useState } from "react";

export const useForm = (initialState = {}) => {
  const [formValues, setFormValues] = useState(initialState);

  const handleInputChange = ({ target }) => {
    setFormValues({
      ...formValues,
      [target.name]: target.type === "checkbox" ? target.checked : target.value,
    });
  };

  return [formValues, handleInputChange];
};
